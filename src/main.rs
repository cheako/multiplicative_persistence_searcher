use std::thread;

extern crate num_bigint as bigint;
use bigint::BigUint;
use crossbeam_channel::{Receiver, Sender};

type Packet = Vec<u8>;

// const START: &[u8] = b"277777788888899";
const START: &[u8] = b"9918188888771117117111727";
// const START: &[u8] = b"2777777888888933";
// const START: &[u8] = b"2777777888882499";
// const START: &[u8] = b"27777778888824933";

fn worker(s: Sender<String>, r: Receiver<Packet>) {
    let zero = BigUint::from(0 as u8);
    let two = BigUint::from(2 as u8);
    let three = BigUint::from(3 as u8);
    let five = BigUint::from(5 as u8);
    let seven = BigUint::from(7 as u8);
    while let Ok(p) = r.recv() {
        let mut n = BigUint::parse_bytes(&p, 10).unwrap();
        while n.clone() % two.clone() == zero && n.clone() != zero.clone() {
            n = n.clone() / two.clone();
        }
        while n.clone() % three.clone() == zero.clone() && n.clone() != zero.clone() {
            n = n.clone() / three.clone();
        }
        while n.clone() % five.clone() == zero.clone() && n.clone() != zero.clone() {
            n = n.clone() / five.clone();
        }
        while n.clone() % seven.clone() == zero.clone() && n.clone() != zero.clone() {
            n = n.clone() / seven.clone();
        }
        if n.clone() == zero.clone() {
            let w = format!("Winner: {}", unsafe { String::from_utf8_unchecked(p) });
            s.send(w.clone()).expect(&w);
        }
    }
}

struct NumberIter {
    n: Vec<u8>,
}

impl Iterator for NumberIter {
    type Item = Vec<u8>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut p = self.n.len() - 1;
        loop {
            if self.n[p - 1] < self.n[p] {
                self.n.swap(p, p - 1);
                let l = self.n.len();
                self.n[p..l].sort();
                return Some(self.n.clone());
            }
            p = p - 1;
            if p == 0 {
                self.n.push('1' as _);
                self.n.sort();
                return Some(self.n.clone());
            }
        }
    }
}

fn generator(ps: Sender<Packet>, ss: Sender<String>) {
    let mut iterator = NumberIter { n: START.to_vec() };
    loop {
        for current in iterator.by_ref().take(500_000) {
            ps.send(current).expect("Failed to send current");
        }
        let _ = ss.send(String::from(String::from_utf8_lossy(&iterator.n[..])));
    }
}

fn main() {
    let num = num_cpus::get();
    let (ps, pr) = crossbeam_channel::bounded::<Packet>(num);
    let (ss, sr) = crossbeam_channel::bounded::<String>(num);

    for _ in 0..num {
        let (s, r) = (ss.clone(), pr.clone());
        thread::spawn(move || worker(s, r));
    }

    thread::spawn(move || generator(ps, ss));

    loop {
        println!("{}", sr.recv().unwrap());
    }
}
